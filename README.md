# noshow.sh

[![ade with Shell Script](https://img.shields.io/badge/made%20with-Shell%20Script-1F1A1C.svg?logo=gnubash&logoColor=white)](https://en.wikipedia.org/wiki/Shell_script)
[![licensed CC0 1.0](https://img.shields.io/badge/licensed-CC0%201.0-44B987.svg?logo=unlicense&logoColor=white)](https://creativecommons.org/publicdomain/zero/1.0/deed.en)

## CAUTION

This Bash Script is a work in progress.

Do **NOT** use this if you cannot read and understand the source code.

Do **NOT** use this without understanding the browser cache.

I have only tested it with Firefox and Chromium.

## What's this?

It generates a tiny single HTML file.

For things like access tokens and private keys, you can store them locally but not have them appear on the screen when you use them.

It is not for everyday use, but to help people realize that "ok, surely with something like this, the token does not appear on the display".

## Usage

keyboard input:

```
$ ./noshow.sh --label='access token'
accepted label: access token 
input something like token or key: 
done.
```

STDIN:

```
$ grep '^wif=' key.txt | head -1 | sed -e 's/^wif="//' -e 's/"[^"]*$//' | ./noshow.sh --label=privkey1
accepted label: privkey1 
(STDIN has been captured)
done.
```

generate a password automatically, and then no human ever sees it or types it into any keyboard:

```
$ dd if=/dev/urandom bs=1 count=20 2>/dev/null | base64 | ./noshow.sh --label='password for example.com'
accepted label: password for example.com
(STDIN has been captured)
done.
```

## TODO

- fix legacy style "document.execCommand('copy')"
- search something like "-webkit-text-security"
- decide on how to deal with multi-byte character
- decide on how to deal with multi-line data
